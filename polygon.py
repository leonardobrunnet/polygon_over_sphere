#!/usr/bin/env python
import torch
import numpy as np
#from mpl_toolkits.basemap import mplot3d
import time
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import sys
from func import polygon
from func import read_state, write_state, func_d_lamb
from func import convert_cartesian_to_spherical, convert_cartesian, generate_polar_circle
from func import generate_circle_at_theta_phi, rotate_around_y, rotate_around_z, rotate_from_zenite_to_theta_phi
#from func import verify
from func import generate_image_points, make_3d_image, make_3d_image_cm
from func import neighbors, search_maxforce, init #,division
from func import generate_cell_close_to_south_pole, convert_cartesian_tensor_to_spherical, generate_tangent_field_to_north
from func import move_from_south_until_contact, concatenate_neighbors_list, calculate_all_forces
from func import print_intt, generate_new_cell_and_migrate_to_north, init_image

#device = torch.device("cuda" if torch.cuda.is_available() else "cpu")                     

#main program
t,tf,division_time,finish_division,poly_list,param_list,movie_file,device = init()

v0, D, dt0, ka , kb, ks, kf,  Req,  Req_inter, R0, R0_inter ,A0, p0, new_p0, Frep, Fadh, mu, num_part, r, Rs0, tau = [i for i in param_list]
dt = dt0
kf0 = kf
next_division=division_time+t
division_axis = 1
intt=int(t)
t0=time.time()
fig,ax=init_image(Rs0)
division_axis = 0
propulsion = False
while t < tf:
    if t > finish_division : propulsion = True
    p0,A0,fmax,poly_list=calculate_all_forces(r,p0,new_p0,A0,kb,kf,poly_list,device)
    dt=min(0.001/fmax,dt0)
    t+=dt
    intt=print_intt(t,intt,dt,ax,fig,poly_list,movie_file,Rs0)
    list(map(lambda i:i.move_cell_particles(dt,propulsion),poly_list))
    division_axis,next_division,poly_list=generate_new_cell_and_migrate_to_north(t,kf0,next_division,division_time,finish_division,division_axis,poly_list,param_list,propulsion,device)
for i in poly_list:
   print(i.ident,i.neighbor_list)
make_3d_image(fig,ax,poly_list,Rs0)
make_3d_image_cm(fig,ax,poly_list)
plt.show()
write_state(t,poly_list)








