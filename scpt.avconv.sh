#!/bin/bash
if [ -z $1 ]
then
echo "please type a movie name"
exit
else
ffmpeg -r 10 -i %4d.png -vcodec libx264 -acodec aac -vf scale=-1:700 $1
fi

