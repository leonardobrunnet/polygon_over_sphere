import torch
import numpy as np
#from polygonclass import polygon
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

######################################################################################################

#initialization and all forces

######################################################################################################

def init():
    device=torch.device("cuda" if torch.cuda.is_available() else "cpu")
    #device="cpu"
    print("device=",device)
    with open("param.txt","r+") as in_file :
        initialization=int(in_file.readline().split()[0])
        in_file.readline()  #jump commented line in param.txt
        lineread = list(map(float,in_file.readline().split()))
        tf,division_time,finish_division = [i for i in lineread]
        in_file.readline()  #jump commented line in param.txt
        param_float=list(map(float,in_file.readline().split()))
        v0,D,dt,tau,ka,kb,ks,kf,Req,Req_inter,R0,R0_inter,Frep,Fadh,p0,new_p0,num_part=[i for i in param_float]
        num_part = int(num_part)
        kb = kb*num_part
        t=0.
        mu = 1.
        dt0 = dt
        r=torch.tensor(num_part*Req/(2*np.pi)) #polygon "radius"
        A0 = (num_part*Req/p0)**2 #target area
        Rs0 = 10*r       #sphere radius
        param_list=[v0, D, dt0, ka , kb, ks, kf,  Req,  Req_inter, R0, R0_inter ,A0, p0, new_p0, Frep, Fadh, mu, num_part, r, Rs0, tau]        

    if initialization == 0 :
        # Determine maximum number of polygons
        max_poly = int(4*Rs0**2/r**2)
        #set max number of polygons
        poly = 1
        if poly > max_poly : 
            print("Too much cells on the sphere, exiting...")
            exit()
        #Generate polygons on the sphere
        poly_list = []
        X=generate_polar_circle(r,Rs0,num_part,device) #circle centered at zenith
        X = X.to(device) #desnecessario
        poly_list.append(polygon(0,X,device,param_list)) #first cell
        movie_file=open("positions_for_movie.dat","w+")

    if initialization == 1 :
        t,poly_list=read_state(param_list,device)
        movie_file=open("positions_for_movie.dat","a+")
        
    movie_file.write("num_part %d Rs0 %f\n"%(num_part,Rs0))
    return t,tf,division_time,finish_division,poly_list,param_list,movie_file,device

def calculate_all_forces(r,p0,new_p0,A0,kb,kf,poly_list,device):
    list(map(lambda i:i.zero_forces(),poly_list))
    list(map(lambda i:i.perimeter_and_self_repulsion_forces(),poly_list))
    if kb > 0: list(map(lambda i:i.bending_force(),poly_list))
    list(map(lambda i:i.center_of_mass(),poly_list))
    list(map(lambda i:i.area_force(),poly_list))
    list(map(lambda i:i.field_to_north_pole(kf),poly_list))
    neighbors(poly_list,r,device)
    list(map(lambda i:i.inter_polygon_forces(poly_list),poly_list))
    fmax=search_maxforce(poly_list)
    if kb > 0:    
        if t > finish_division and t < finish_division+dt:
            print("Finished dividing. Reseting kb to zero")
            kb=0.
            p0=new_p0
            A0=(num_part*Req/p0)**2
    return p0,A0,fmax,poly_list


################################################################################################

#Squared distance matrices, search neighbor polygons, divide polygons, search max force to adapt dt

################################################################################################

def dist_mat_square(X):
    return torch.sqrt(torch.sum((X[:,None,:]-X[None,:,:])**2,axis=2))

def distmat_square_inter_polygons(X,Y):
    return torch.sqrt(torch.sum((X[:,None,:]-Y[None,:,:])**2,axis=2))

def neighbors(poly_list,r,device):
    lp=len(poly_list)
    X_cm=torch.zeros(lp,3,device=device)
    for i in poly_list:
        X_cm[i.ident]=i.center_of_mass()
    D=dist_mat_square(X_cm)
    D += torch.eye(lp,device=device)*10**4
    one = torch.ones(lp,device=device)
    zero = torch.zeros(lp,device=device)
    D = torch.where(D<(3.6*r),one,zero)
    z=torch.tensor(np.arange(1,lp+1),device=device)
    for i in poly_list:
        y = list(map(int,((D[i.ident]*z)).tolist()))
        y = list(filter(lambda num: num != 0, y[i.ident:]))
        y = list(i-1 for i in y)
        i.neighbor_list=y
    return
    
def concatenate_neighbors_list(poly_list,r,device):
    total_neighbor_list=[]
    neighbors(poly_list,r,device)
    for i in poly_list:
        total_neighbor_list+=i.neighbor_list
    return total_neighbor_list

# def division(poly_list,r,division_axis,param_list,device):
#     pi = torch.tensor(np.pi)
#     number_of_cells = len(poly_list)
#     mother_cell = np.random.randint(number_of_cells)
#     num_part = len(poly_list[mother_cell].X)
#     Xcm_mother = poly_list[mother_cell].cm
#     R,theta,phi = convert_cartesian_to_spherical(Xcm_mother)
#     dphi=torch.arcsin(1.1*r/(2*R))
#     X_mother=generate_circle_at_theta_phi(num_part,r/2.5,R,division_axis*pi/2,dphi,device) #at zenit
#     X_mother=rotate_from_zenite_to_theta_phi(X_mother,theta,phi)
#     poly_list[mother_cell].X = X_mother
#     X_daughter=generate_circle_at_theta_phi(num_part,r/2.5,R,division_axis*pi/2,-dphi,device) #at zenit
#     X_daughter = rotate_from_zenite_to_theta_phi(X_daughter,theta,phi)
#     new_index=len(poly_list)
#     poly_list.append(polygon(new_index,X_daughter,device,param_list))
#     return

def search_maxforce(poly_list):
    list(map(lambda i:i.max_force(),poly_list))
    maxforce_list=[]
    for i in poly_list:
        maxforce_list.append(i.maxforce)
    fmax=max(maxforce_list)

    return fmax       

def move_from_south_until_contact(dt,i,kf,propulsion):
    i.zero_forces()
    i.perimeter_and_self_repulsion_forces()
    i.center_of_mass()
    i.area_force()
    i.field_to_north_pole(kf)
    i.move_cell_particles(dt,propulsion)
    return

def  generate_new_cell_and_migrate_to_north(t,kf0,next_division,division_time,finish_division,division_axis,poly_list,param_list,propulsion,device):
    v0, D, dt0, ka , kb, ks,  kf0, Req,  Req_inter, R0, R0_inter ,A0, p0, new_p0, Frep, Fadh, mu, num_part, r, Rs0, tau = [i for i in param_list]
    if t>next_division and t<finish_division:
        division_axis += 1
        X=generate_cell_close_to_south_pole(num_part,division_axis,r,Rs0,device)
        new_index = len(poly_list)
        poly_list.append(polygon(new_index,X,device,param_list))
        total_neighbor_list=concatenate_neighbors_list(poly_list,r,device)
        print("moving new cell to north pole")
        while new_index not in total_neighbor_list :
            kf=1.0
            move_from_south_until_contact(dt0,poly_list[new_index],kf,propulsion)
            total_neighbor_list=concatenate_neighbors_list(poly_list,r,device)
        print("finish moving")
        kf=kf0
        #division(poly_list,r,division_axis,param_list,device)
        division_axis+=1
        next_division+=division_time
    return division_axis,next_division,poly_list


##############################################################################

# Generate polygons on the sphere and rotate

###############################################################################


def generate_polar_circle(r,Rs,num_part,device):
    rho = torch.arcsin(r/Rs)
    phi = np.pi/2-rho
    X = torch.zeros(num_part,3,device=device)
    dtheta = torch.tensor(2*np.pi/num_part,device=device)
    for i in range(num_part):
        X[i,:]=convert_cartesian(Rs,i*dtheta,phi,device)
    return X

def generate_circle_at_theta_phi(num_part,r,Rs,theta,phi,device):
    delta = phi + torch.tensor(np.pi)/2
    Z = generate_polar_circle(r,Rs,num_part,device)
    Z = rotate_around_y(Z,delta)
    Z = rotate_around_z(Z,theta)
    return Z

def generate_cell_close_to_south_pole(num_part,division_axis,r,Rs0,device):
    print("Generating a cell close to south pole")
    pi = torch.tensor(np.pi)
    phi = torch.arcsin(r/Rs0)-pi #angle close to south pole
    X = generate_circle_at_theta_phi(num_part,r,Rs0,division_axis*pi/7,phi,device)
    return X
    
def rotate_from_zenite_to_theta_phi(Z,theta,phi):
    Z = rotate_around_y(Z,phi)
    Z = rotate_around_z(Z,theta)
    return Z

def rotate_around_y(X,phi):
    delta = torch.tensor(np.pi)/2 - phi
    Y = torch.zeros_like(X)
    Y[:,0] = X[:,0]*torch.cos(delta) + X[:,2]*torch.sin(delta)
    Y[:,1] = X[:,1]
    Y[:,2] = X[:,2]*torch.cos(delta) - X[:,0]*torch.sin(delta)    
    return Y
    
def rotate_around_z(X,theta):
    Y = torch.zeros_like(X)
    Y[:,0] = X[:,0]*torch.cos(theta) - X[:,1]*torch.sin(theta)
    Y[:,1] = X[:,1]*torch.cos(theta) + X[:,0]*torch.sin(theta)
    Y[:,2] = X[:,2]
    return Y


def convert_cartesian_tensor_to_spherical(X):
    Rs = torch.norm(X,dim=1)
    theta = torch.atan2(X[:,1],X[:,0])
    phi = torch.asin(X[:,2]/Rs[:])
    return Rs,theta,phi


def convert_spherical_tensor_to_cartesian(Rs,theta,phi,device):
    x=torch.zeros(len(Rs),3,device=device)
    x[:,0] = Rs[:]*torch.cos(phi[:])*torch.cos(theta[:])
    x[:,1] = Rs[:]*torch.cos(phi[:])*torch.sin(theta[:])
    x[:,2] = Rs[:]*torch.sin(phi[:])
    return x

def convert_spherical_tensor_to_unitary_cartesian(Rs,theta,phi,device):
    x=torch.zeros(len(Rs),3,device=device)
    x[:,0] = torch.cos(phi[:])*torch.cos(theta[:])
    x[:,1] = torch.cos(phi[:])*torch.sin(theta[:])
    x[:,2] = torch.sin(phi[:])
    return x

def convert_spherical_vector_to_unitary_cartesian(theta,phi,device):
    x=torch.zeros(3,device=device)
    x[0] = torch.cos(phi)*torch.cos(theta)
    x[1] = torch.cos(phi)*torch.sin(theta)
    x[2] = torch.sin(phi)
    return x

def generate_tangent_field_to_north(X,device):
    R,theta,phi = convert_cartesian_tensor_to_spherical(X)
    delta = torch.tensor(np.pi,device=device)/2
    phi+=delta
    x = convert_spherical_tensor_to_unitary_cartesian(R,theta,phi,device)
    return x


def random_unitary_tangent_vector(X,device):
    pi2=torch.tensor(np.pi,device=device)/2
    Rs,theta,phi=convert_cartesian_to_spherical(X)
    dz = 2*torch.rand(2,device=device)-1
    x1 = convert_spherical_vector_to_unitary_cartesian(theta+pi2,torch.tensor(0.),device)
    x2 = convert_spherical_vector_to_unitary_cartesian(theta,phi+pi2,device)
    x3 = dz[0]*x1+dz[1]*x2
    x3 /=torch.norm(x3)
    return x3
    
def self_propulsion_angle_and_angular_noise(dt,tau,DR,n,dxcm,xcm,device):
    dxcm /= torch.norm(dxcm)
    xrand = random_unitary_tangent_vector(xcm,device)
    dn = torch.cross(torch.cross(n,dxcm),n)*dt/tau + np.sqrt(2*dt*DR)*xrand
    n += dn
    return n


def force_field_inter_polygons(X,Y,R,zero_tensor,Req_inter,R0_inter,Frep,Fadh):
    force = force_mod(R,zero_tensor,Req_inter,R0_inter,Frep,Fadh)
    FF_target_polygon = torch.sum(force[:,:,None]*((X[:,None,:]-Y[None,:,:])),axis=1)
    FF_reaction = -torch.sum(force[:,:,None]*((X[:,None,:]-Y[None,:,:])),axis=0)
    return FF_target_polygon,FF_reaction

def force_mod(R,zero_tensor,Req,R0,Frep,Fadh):
    frep  = -Frep*(1-Req/R)
    frep  = torch.where(R<Req,frep,zero_tensor)
    fadh  = -Fadh*(1-Req/R)
    fadh  = torch.where(R>Req,fadh,zero_tensor)
    fadh  = torch.where(R<R0,fadh,zero_tensor)
    force = fadh+frep
    return  force



def convert_cartesian_to_spherical(X):
    Rs = torch.norm(X)
    theta = torch.atan2(X[1],X[0])
    phi = torch.asin(X[2]/Rs)
    return  Rs,theta,phi

def convert_cartesian(Rs,theta,phi,device):
    x = torch.zeros(3,device=device)
    x[0] = Rs*torch.cos(phi)*torch.cos(theta)
    x[1] = Rs*torch.cos(phi)*torch.sin(theta)
    x[2] = Rs*torch.sin(phi)
    return x


def func_d_lamb(r,Rs,delta):
    L = Rs * torch.sin(delta)
    d_lamb = torch.arcsin(2.1*r/L)
    return d_lamb
    


##################################################################################################

# Classes

#################################################################################################


class polygon:
    
    def __init__(self,ident,X,device,param_list):
        v0, D, dt0, ka , kb, ks,  kf, Req,  Req_inter, R0, R0_inter ,A0, p0, new_p0, Frep, Fadh, mu, num_part, r, Rs0, tau = [i for i in param_list]
        self.X = X.clone().detach().to(device)
        self.x = X.clone().detach().to(device) #this will be the tangent vector pointing to north
        self.ident = ident
        self.num_part = len(X)
        self.Force = torch.zeros(self.num_part,3,device=device)
        self.neighbor_list = []
        self.maxforce = 0
        self.ks = ks
        self.kb = kb
        self.Req = Req
        self.Req_inter = Req_inter
        self.Frep =Frep
        self.Fadh = Fadh
        self.mu = mu
        self.R0 = R0
        self.R0_inter = R0_inter
        self.device = device
        self.A0 = A0
        self.Rs0 = Rs0
        self.ka = ka
        self.cm = self.center_of_mass()
        self.n = random_unitary_tangent_vector(self.cm,device)
        self.D = D
        self.v0 = v0
        self.tau = tau

    def move_cell_particles(self,dt,propulsion):
        # dz = random_unitary_tangent_vector(self.cm,self.device) #including translational noise in the cm
        # z = dz.repeat(self.num_part,1)
        dx  = self.mu*self.Force*dt  #+ self.D*z*np.sqrt(dt)
        #########################self propulsion, persistence and angular noise#####################
        if propulsion :
            dxcm = torch.sum(dx,0)
            self.n=self_propulsion_angle_and_angular_noise(dt,self.tau,self.D,self.n,dxcm,self.cm,self.device)
            z = self.n.repeat(self.num_part,1)
            dx += self.v0*z*dt
        ###########################self propulsion with angular noise############
        # if propulsion :
        #     dn = random_unitary_tangent_vector(self.cm,self.device)
        #     self.n += dn*np.sqrt(self.D*dt)
        #     self.n /= torch.norm(self.n)
        #     z = self.n.repeat(self.num_part,1)
        #     dx += self.v0*z*dt
        ###################################################################################    
        self.X += dx
        #back to the surface
        R,theta,phi = convert_cartesian_tensor_to_spherical(self.X)
        R=torch.ones(self.num_part,device=self.device)*self.Rs0
        self.X=convert_spherical_tensor_to_cartesian(R,theta,phi,self.device)
        # print(c)
        return




    def perimeter_and_self_repulsion_forces(self):
        Xp = torch.cat((self.X[:,:],self.X[None,0,:]),0)#adding first particle position to the end
        self.Xd = Xp[1:,:]-self.X   #right distance between particles
        self.Xe = -torch.cat((self.Xd[None,-1,:],self.Xd[:-1,:]),0) #left distance between particles
        R = torch.sqrt(torch.sum(self.Xd**2,axis=1))
        fnormd = self.ks*(R-self.Req)
        fnorme = torch.cat((fnormd[None,-1],fnormd[:-1]))
        f_vec_d = self.Xd * torch.div(fnormd,torch.norm(self.Xd,dim=1))[:,None]
        f_vec_e = self.Xe * torch.div(fnorme,torch.norm(self.Xe,dim=1))[:,None]  
        self.Force = f_vec_d + f_vec_e
        #Repulsion if not first neighbors when too close
        R = dist_mat_square(self.X)
        z = torch.ones(self.num_part,device=self.device)
        tridiag = (torch.diag(z[:-1],1)+torch.diag(z)+torch.diag(z[:-1],-1))*1000 # large tridiagonal matrix
        tridiag[-1,0]+=1000
        tridiag[0,-1]+=1000
        tridiag = tridiag.to(self.device)
        R += tridiag #summing a large tridiagonal to avoid auto zero distance and already calculated neighbors
        zero_tensor = torch.zeros(self.num_part,self.num_part,device=self.device)
        #FF = force_field_intra_polygon(self.X,R,zero_tensor,self.Req,self.Frep)
        FF = self.force_field_intra_polygon(R,zero_tensor)
        self.Force += FF

        return

    def area_force(self):
        Xaux = self.X-self.cm
        Xp = torch.cat((Xaux[:,:],Xaux[None,0,:]),0)#adding first particle position to the end
        self.area=torch.sum(torch.linalg.norm(torch.cross(Xp[1:,:],Xp[:-1,:],axis=1),dim=1))/2
        self.Force += -torch.div(Xaux,torch.norm(Xaux,dim=1)[:,None])*(self.area-self.A0)*self.ka
        # if self.ident == 0:
        #     print(self.ident,self.area,self.A0)
        return
        
    
    def bending_force(self):
        X_perp_d=torch.cross(self.Xd,self.X,axis=1) #vector perpendicular to membrane right
        X_perp_e=torch.cross(self.X,self.Xe,axis=1) #vector perpendicular to membrane left
        X_perp_d=torch.div(X_perp_d,torch.norm(X_perp_d,dim=1)[:,None]) #normalizing
        X_perp_e=torch.div(X_perp_e,torch.norm(X_perp_e,dim=1)[:,None]) #normalizing        
        self.angle = torch.arccos(torch.sum(X_perp_d*X_perp_e,axis=1)) #scalar product of unitary vectors
        X_perp_sum = X_perp_e+X_perp_d  #central bending vector
        X_perp_sum = torch.div(X_perp_sum,torch.norm(X_perp_sum,dim=1)[:,None]) #normalizing
        self.Force += -self.kb*self.angle[:,None]*X_perp_sum    #bending force on focus particle
        index_d=list([i for i in range(1,len(self.angle))])+[0]     
        index_e=list([i for i in range(-1,len(self.angle)-1)])       
        #forces on focus site and first neighbor should sum zero
        self.Force[index_d] +=  self.kb*self.angle[:,None]*X_perp_d/(2.*torch.cos(self.angle[:,None]/2.)) 
        self.Force[index_e] +=  self.kb*self.angle[:,None]*X_perp_e/(2.*torch.cos(self.angle[:,None]/2.))
        return 

    def max_force(self):
        self.maxforce=max(torch.norm(self.Force,dim=1)).item()
        return


    def field_to_north_pole(self,kf):
        self.cm = self.center_of_mass()
        #print(self.cm)
        z=self.cm.repeat(self.num_part,1)
        self.x = generate_tangent_field_to_north(z,self.device)
        self.Force += kf*self.x
        return
    
    
    def zero_forces(self):
        self.Force = torch.zeros(self.num_part,3,device=self.device)
        return
    
    def center_of_mass(self):
        self.cm = torch.sum(self.X,0)/len(self.X)
        return self.cm
    
    def inter_polygon_forces(self,poly_list):
        for i in self.neighbor_list :
            Y = poly_list[i].X
            R = distmat_square_inter_polygons(self.X,Y)
            zero_tensor=torch.zeros(self.num_part,self.num_part,device=self.device)
            myForce,reactionForce=force_field_inter_polygons(self.X,poly_list[i].X,R,zero_tensor,self.Req_inter,self.R0_inter,self.Frep,self.Fadh)
                                                          #force_field_inter_polygons(X,Y,R,zero_tensor,Req,R0,Frep,Fadh         

            self.Force+=myForce
            poly_list[i].Force+=reactionForce       
        return

    
    def force_field_intra_polygon(self,R,zero_tensor):
        force = self.force_mod_internal_polygon(R,zero_tensor)
        # if self.ident == 1:
        #     print(force)
        #     exit()

        FF = torch.sum(force[:,:,None]*((self.X[:None,:]-self.X[None,:,:])),axis=1)
        return FF

    def force_mod_internal_polygon(self,R,zero_tensor):
        frep = -self.Frep*(1-self.Req/R)
        frep = torch.where(R<self.Req,frep,zero_tensor)
        return frep

################################################################################################

# Input output

################################################################################################

def read_state(param_list,device):
    with open("state.txt","r+") as out_state :
        t=float(out_state.readline().split()[0])
        poly_list = []
        while 1 :
            line = out_state.readline()
            if not line:
                X = torch.tensor(X)
                poly_list.append(polygon(poly_index,X,device,param_list))
                break
            line_splitted = line.split()
            if line_splitted[0] == "#" :
                poly_index = int(line_splitted[1])
                if poly_index > 0 :
                    X = torch.tensor(X)
                    poly_list.append(polygon(poly_index-1,X,device,param_list))
                    #print(X)
                #print(poly_index)
                X=[]
            else:
                x=list(map(float,line_splitted))
                X.append(x)
                #print(x)
    return t,poly_list
    
    

def write_state(t,poly_list):
    out_state=open("state.txt","w+")
    out_state.write(" %f \n"%t)
    for i in poly_list:
        out_state.write("# %d\n"%i.ident)
        for x in i.X:
            out_state.write("%f %f %f\n"%(x[0],x[1],x[2]))
    return

def write_partial_state(poly_list,movie_file):
    for i in poly_list:
        movie_file.write("# %d\n"%i.ident)
        for x in i.X:
            movie_file.write("%f %f %f\n"%(x[0],x[1],x[2]))
    movie_file.write("End image\n")
    return


def verify(question):
    print(question)
    while True :
        line_splitted = sys.stdin.readline().split()
        answer = int(line_splitted[0])
        if answer == 0 or answer == 1:
            return answer
        else:
            print("Please say 0 or 1")
    return
    
# def write_param(param,r):
#     param.write(" dt, ka, kb, ks, R0, Req, Rs0, Frep, Fadh, mu, r, p0 is separated because we may change it\n")
#     param.write("%f %f % f %f %f %f %f %f %f %f %f \n "%(dt, ka, kb, ks, R0, Req, Rs0, Frep, Fadh, mu, r))
#     param.write("%f "%p0)
#     return

#################################################################################################

#generating images

#################################################################################################

def init_image(Rs0):
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    plt.xlim(-Rs0,Rs0)
    plt.ylim(-Rs0,Rs0)
    ax.set_zlim(-1.1*Rs0,1.1*Rs0)
    ax.view_init(60,0)
    return fig,ax


def make_3d_image(fig,ax,poly_list,Rs0):
    plt.xlim(-Rs0,Rs0)
    plt.ylim(-Rs0,Rs0)
    ax.set_zlim(-1.1*Rs0,1.1*Rs0)
    ax.view_init(60,0)
    for i in poly_list:
        xs,ys,zs=generate_image_points(i.X)
        xxs,yys,zzs=generate_image_points(i.x)
        xs,ys,zs=xs.to("cpu"),ys.to("cpu"),zs.to("cpu")
        xxs,yys,zzs=xxs.to("cpu"),yys.to("cpu"),zzs.to("cpu")
        ax.scatter(xs,ys,zs)
        #ax.quiver(xs,ys,zs,xxs,yys,zzs,length=1.5, normalize=True)
    return 


def make_3d_image_cm(fig,ax,poly_list):
#     fig = plt.figure()
#     ax = fig.add_subplot(projection='3d')
    # plt.xlim(-15,15)
    # plt.ylim(-15,15)
    # ax.set_zlim(30,50)
    # ax.zaxis.set_major_locator(ticker.MultipleLocator(10))q
    for i in poly_list:
        xs,ys,zs=i.center_of_mass().cpu()
        #ax.scatter(xs,ys,zs)
        ax.text(xs,ys,zs,"%s"%str(i.ident))
    #ax.view_init(np.pi/4,np.pi/4)
#plt.show()

def generate_image_points(X):
    xs = X[:,0]
    ys = X[:,1]
    zs = X[:,2]
    return xs,ys,zs


def print_intt(t,intt,dt,ax,fig,poly_list,movie_file,Rs0):
    if int(t)>intt:
        intt=int(t)
        print("t=%d dt=%f"%(intt,dt))
        make_3d_image(fig,ax,poly_list,Rs0)
        plt.pause(0.1)
        plt.cla()
        write_partial_state(poly_list,movie_file)
        
    return intt
