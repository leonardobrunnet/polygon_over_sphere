import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
def init_image(Rs0):
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    plt.xlim(-Rs0,Rs0)
    plt.ylim(-Rs0,Rs0)
    ax.set_zlim(-1.1*Rs0,1.1*Rs0)
    ax.view_init(60,0)
    return fig,ax

with open("positions_for_movie.dat","r+") as in_file :
    param = in_file.readline().split()
    num_part,Rs0 = int(param[1]),float(param[3])
    fig,ax = init_image(Rs0)
    image_number=0
    while 1 :
        line = in_file.readline().split()
        if not line :
            #plt.show()
            break
        mark,ident = line[0],line[1]
        while mark == "#" :
            x,y,z = [],[],[]
            for i in range(num_part):
                r=list(map(float,in_file.readline().split()))
                x.append(r[0]),y.append(r[1]),z.append(r[2])
            ax.scatter(x,y,z)
            mark,ident = in_file.readline().split()
        if mark == "End" :
            plt.xlim(-Rs0,Rs0)
            plt.ylim(-Rs0,Rs0)
            ax.set_zlim(-1.1*Rs0,1.1*Rs0)
            number=str(image_number).zfill(4)
            name="figs/"+number+".png"
            plt.savefig(name)
            image_number+=1
            print(number)
            plt.pause(0.1)
            plt.cla()
#ax.scatter(x,y,z)
#plt.show()
    
    
    
    
